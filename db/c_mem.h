#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "hash.h"
#include <stdint.h>
#include <iostream>
#include <list>


//256K 为1页，如果一个页放不下某一帧数据，放到下一页




struct s_data{
	char *v_key;
	int v_keysize = 0;
	char *v_value= NULL;
	int v_valuesize = 0;
};

//载入128M内存
struct s_block
{
	uint8_t* v_data = NULL;
	uint32_t v_len = 0;
	int v_pagenum = 0;
};
//需要的内存页
struct s_page{
    
	char is_dirty:1; //是否为脏数据,发送时可能修改增加头部字节或尾部字节
	char is_full:1; //是否满
	char is_empty:1; //是为空
	char is_lock:1;  //是链接页
	char is_hasnext:1;//是否有下一页,末尾8字节为下一页地址
	char is_ok:1;
	//2 bits is status
	char status:2;


	//第一个字节需要存储基本信息，非实际内容
	char* v_sphi = NULL; //实际加载内存地址
	char* v_r = NULL;    //相对地址，实际移动一字节
	char* v_w = NULL;    //相对地址
		//页内索引个数，每一对占8字节
	//不够则增加一页挂在链表上
	s_page* v_next = NULL;

	//预留四字节为下一页地址
};
struct s_mem
{
	char* v_mem;
	//内存地址长度
	int v_size = 0;
	char* v_w;//写地址
};
class c_mem
{
	char* v_mem_index = NULL;
	//和操作系统页面默认大小相同
	int v_psize = 4096;

	std::vector<s_mem*> v_order;
	int v_prime = 1023;
	
	char* v_mem_content = NULL;
	char* v_mem_content_w = NULL;
public:
	//create(20*1024*1024 ， 4096） 4096 可以放多少个8字节指针 4096/8 = 512个索引值
	//                                                         4字节hash，4字节偏移地址
	// 20M有5120页 每个页能放512 个索引 一共 5120 * 512
	 
	//操作系统页面大小 4096
	//num 为4096的倍数
	void createindex(int num, int pagesize)
	{
		v_mem_index = new char[num];
		v_psize = pagesize;
		int p = num / pagesize; 
		//if mem is 20*1024*1024 p = 5120 0~5119
		//if mem is 100*1024*1024 p = 5120*5 =  25600 0~25599
		v_prime = hash_next_prime(p);
		std::cout << "v_prime is" << v_prime << std::endl;
	}

	void createcontent(int num)
	{
		v_mem_content = new char[num];
		v_mem_content_w = v_mem_content;
	}
	void storage(std::string key, std::string content)
	{
		int num = hash_add(key.c_str(), v_prime);
	}

};

