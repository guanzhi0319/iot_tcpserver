#include "c_read_async.h"

static char buffer[1024];

static uv_buf_t iov;
uv_fs_t open_req;
uv_fs_t read_req;
uv_fs_t write_req;
uv_async_t async;
int g_status = -1;

void on_read(uv_fs_t* req);

void on_write(uv_fs_t* req) {
    if (req->result < 0) {
        fprintf(stderr, "Write error: %s\n", uv_strerror((int)req->result));
    }
    else {
        uv_fs_read(uv_default_loop(), &read_req, open_req.result, &iov, 1, -1, on_read);
    }
}

void on_read(uv_fs_t* req) {
    if (req->result < 0) {
        fprintf(stderr, "Read error: %s\n", uv_strerror(req->result));
    }
    else if (req->result == 0) {
        uv_fs_t close_req;
        // synchronous
        uv_fs_close(uv_default_loop(), &close_req, open_req.result, NULL);
    }
    else if (req->result > 0) {
        iov.len = req->result;
        uv_fs_write(uv_default_loop(), &write_req, 1, &iov, 1, -1, on_write);



    }
}

void on_open(uv_fs_t* req) {
    // The request passed to the callback is the same as the one the call setup
    // function was passed.
   // assert(req == &open_req);

    if (req->result >= 0) {
        iov = uv_buf_init(buffer, sizeof(buffer));
        uv_fs_read(uv_default_loop(), &read_req, req->result,
            &iov, 1, -1, on_read);
    }
    else {
        fprintf(stderr, "error opening file: %s\n", uv_strerror((int)req->result));
    }
}


void async_cb(uv_async_t* handle)
{
    printf("async_cb called!\n");
    uv_thread_t id = uv_thread_self();
    printf("thread id:%lu.\n", id);
    //uv_close((uv_handle_t*)&async, close_cb);	//如果async没有关闭，消息队列是会阻塞的
}

void sub_thread(void* arg)
{
    uv_thread_t id = uv_thread_self();
    printf("sub thread id:%lu.\n", id);
    db_context* context = (db_context*)arg;
    const char* fs = "./test.db";
    s_async_context* async_c = (s_async_context*)context->v_user_context;
    uv_fs_open(uv_default_loop(), &async_c->open_req, fs, O_RDONLY, 0, on_open);
    uv_async_send(&async_c->async);
}

void read_init_async(db_context* context, s_async_context* async, const char* fs)
{
    context->v_user_context = async;
    uv_async_init(uv_default_loop(), &async->async, async_cb);
    //call_async()
    uv_thread_t thread;
    uv_thread_create(&thread, sub_thread, context);
   
    uv_run(uv_default_loop(), UV_RUN_DEFAULT);

}


//int main(int argc, char** argv) {
//
//    const char* fs = "./test.db";
//    uv_fs_open(uv_default_loop(), &open_req, argv[1], O_RDONLY, 0, on_open);
//    uv_run(uv_default_loop(), UV_RUN_DEFAULT);
//
//    uv_fs_req_cleanup(&open_req);
//    uv_fs_req_cleanup(&read_req);
//    uv_fs_req_cleanup(&write_req);
//    return 0;
//}