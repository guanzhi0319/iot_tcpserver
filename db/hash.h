#pragma once
#include <string.h>
#include <cmath>
#include <thread>
//注意prime是素数
static int rotating_hash(const char *key, int prime)
{
	size_t hash, i;
	size_t len = strlen(key);
	for (hash = len, i = 0; i < len; i++)
	{
		hash = (hash << 4 >> 28) ^ key[i];
		return (hash % prime);
	}
}

static int hash_add(const char* key, int prime)
{

	size_t hash, i;
	size_t len = strlen(key);
	for (hash = len, i = 0; i < len; i++)
		hash += key[i];
	return (hash % prime);
}

//求核数后面最大的素数
//只能被自己和1整除的数为素数

static int hash_next_prime(int num) {
	if (num == 1)
		return 2;
	if (num == 2)
		return 3;
	if (num % 2 == 0)
		++num;
	int i;
	bool flag = false;
	while (1)
	{
		flag = false;
		for (i = 3; i<num; i += 2)
			if (num % i == 0) {
				flag = true;
				break;
			}
		if (!flag)
			return num;
		num += 2;
	}
}
//寻找反向最大的素数
static int find_min_s(int a) {
	int m = 0;
	for (int i = a; i >0; i--) {
		m = 0;
		for (int j = 2; j <= std::sqrt(i); j++) {
			if (i%j == 0) {
				m = 1;
				break;
			}

		}
		if (m == 0) {
			//std::cout << i << " was the max prime";
			return i;
		}
	}
	if (m == 0) {
		//std::cout << a << " was the max prime";
		return a;
	}
}

static int core_thread()
{
	static int g_thread_core = 0 ;
	if (g_thread_core > 0)
		return g_thread_core;
	int cpu_core = std::thread::hardware_concurrency();
	g_thread_core = hash_next_prime(cpu_core);
	return g_thread_core;
}


//index 为内存地址
static uint32_t find_address(uint32_t key,uint8_t * index, int len)
{

	//先存索引的大小 ，这样可以快速找到索引的位置 rownum
	//512M 字段 个数 字段hash 4字节 字段地址 4字节
	//1M 字节 1024*1024 /8 = 256*256 65536 64K值-
	//2M 字节 2 *1024*1024 /8 = 128k 
	//10M 字节 640K个值 655360 个地址
	//

	//使用二分查找
	//读取存取的索引数据个数
	uint32_t num;
	memcpy(&num, index, sizeof(int)); //如有64万个索引值

}


