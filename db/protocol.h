#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

/*
author : qianbo
email  : 418511899@qq.com
func   : protocol
time   ：2021-12-12
*/
#include "stdio.h"
#include "stdint.h"
#include "uv.h"

/*
   大端还是小端
*/

int checkCPUendian()
{
	union {
		unsigned long int i;
		unsigned char s[4];
	}c;
	c.i = 0x12345678;
	return (0x12 == c.s[0]);
}



typedef enum endian_{
	enum_big = 0,
	enum_little,
}endian_;
/*
  是否在线
*/
typedef enum online_
{
	enum_online = 1,
	enum_offline = 0
}online_;

typedef enum recv_status_
{
	enum_head = 0, //接收头部
	enum_body = 1  //接收包体
}recv_status;

typedef enum packtype_
{
	enum_none = 0,
	enum_cs = 1,       //接收服务器包
	enum_sc = 2,       //发送数据包
	enum_control = 3,  //控制
	enum_config  = 4,  //设置
	enum_query   = 5,  //查询
	enum_total = 6
}packtype_;


typedef struct tcp_unit
{
	//真实数据
	char * bodydata = NULL;
	//数据总长度bodylen + headlen
	int bodylen = 0;
	//接收到的数据长度
	int recvlen = 0;
	//头部长度
	int headlen = 0;
}tcp_unit;



typedef struct tcp_unit_w{
	char * data = NULL;
	int len = 0;
	packtype_ type = enum_cs;
	uv_tcp_t *tcphandle = NULL;
	//延迟多少毫秒发送
	int delay = 0;
	struct tcp_unit_w *next = NULL;
}tcp_unit_w;

typedef struct urls
{
	char * url;
	urls * next;
}urls;

typedef struct pack_pro
{
	char type : 2;//发布和订阅 心跳
	char dtype : 4;
	char v : 2;
	uint8_t  titlelen;
	uint32_t bodylen;
}pack_pro;
//协议头部和回调函数设置
typedef struct tcp_settings
{
	pack_pro pp;
	uv_loop_t * uv_loop = NULL;
}tcp_settings;



static void free_unit(tcp_unit ** unit)
{
	if ((*unit) != NULL) {
		if ((*unit)->bodydata != NULL) {
			free((*unit)->bodydata);
			(*unit)->bodydata = NULL;
		}
		free(*unit);
		*unit = NULL;
	}
}

typedef struct client_t {
	//tcp client session
	uv_tcp_t tcp;
	//解析使用
	tcp_settings * config = NULL;
	//写入
	uv_write_t write_req;


	//设备id
	uint32_t   deviceid = 0;
	//读入的内容
	tcp_unit   *buffer_data = NULL;
	
	//需要写入的内容
	tcp_unit_w *buffer_data_w = NULL;
	
	//最长6字节包头
	char head[6];
	//已经接收的头部的长度
	int recvlen;

	//用户自定义数据指针 tcpserver
	void * data = NULL;
	
	//接收状态 接收头部，0 接收数据 1
	recv_status status = enum_head; 

	//是否在线
	int is_online = enum_online;

	uv_timer_t  _timer;
	uv_thread_t _thread;


	int thread_run = 0;

	int time_init() {
		if (config == NULL)
			return -1;
		uv_timer_init(config->uv_loop, &_timer);
		return 0;
	}

	client_t() {
	}

	void clean()
	{
		free_unit(&buffer_data);
	}
	int headlen() {
		return recvlen;
	}
}client_t;


typedef struct thread_work {
	thread_work(client_t* cli, tcp_unit * unit) :
		request(),
		client(cli),
		data(unit),
		error(false) {
		//保存数据指针,传到处理线程
		request.data = this;
	}
	uint32_t id =0;
	client_t* client = NULL;
	//把数据接过来进行处理
	tcp_unit * data = NULL;
	uv_work_t request;
	bool error;
}thread_work;

//我错了，应该使用js来解析这个
static int get_headlen(tcp_settings * setting)
{
	return 6;
}

static uint32_t get_bodylen(tcp_settings * setting, char *head)
{
	//headlen_offset 是偏移量+1
	int len1 = *(head + 1);
	char *b = head + 2;
	return len1+ntohl(*(uint32_t*)b);
}



//static int get_bodylen2(tcp_settings2 * setting, char *head)
//{
//	//headlen_offset 是偏移量+1
//	if (setting == NULL || head == NULL)
//		return -1;
//	char *pos = head + setting->blen_offset;// -1;
//	int blen_len = setting->blen_len;
//	int ret = 0;
//	if (blen_len == 1) {
//		ret = *(pos);
//	}
//	else if (blen_len == 2) {
//		uint16_t* pos1 = (uint16_t*)(pos);
//		ret = ntohs(*pos1);
//	}
//	else if (blen_len == 4) { //最多四个字节长度
//		uint32_t * pos1 = (uint32_t*)(pos);
//		ret = ntohl(*pos1);
//	}
//	return ret;
//}

#endif