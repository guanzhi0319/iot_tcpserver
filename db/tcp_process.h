#pragma once
#include "tcp_server.h"
#include <unordered_map>
class tcp_server1 :public tcp_server
{
	//所有客戶端，用四字节整形数作为hash key
	std::unordered_map<uint32_t, client_t*>  v_map_c;
	uv_mutex_t  _mutex_1;
public:
	tcp_server1() {}
	~tcp_server1() {}
protected:
	uint32_t getid(client_t* client)
	{
		return 0;
		/*tcp_settings* cnf = client->config;
		char* idpos = &client->head[0] + cnf->idoffset;
		uint32_t deviceid = htonl(*((uint32_t*)idpos));
		///((deviceid >> 16) & 0xff00) | deviceid >> 24
		return deviceid;*/
	}
public:



	int on_headers_complete(void* param) {
		//client_t* pclient = (client_t*)param;

		////printf("the header len is %d\n", pclient->recvlen);
		//printf("the id is %04x\n", getid(pclient));
		client_t* cl = (client_t*)param;
		//得到头部字节
		char head = cl->head[0];
		char type = head >> 6;
		switch (type)
		{
		case 0x00://00 发布数据
			cout << "publish" << endl;
			break;
		case 0x01://01 订阅数据
			cout << "subscribe" << endl;
			break;
		case 0x02://10 心跳数据
			cout << "heartbeat" << endl;
			break;
		}
		return 0;
	}
	//该函数没有进入线程池
	int on_message_complete(void* param) {
		client_t* pclient = (client_t*)param;
		tcp_unit* data = pclient->buffer_data;
		//char* buf = data->data;
		//int len = data->tlen;
		//printf("the total len is %d\n", pclient->buffer_data->tlen);
		return 0;
	}
	//该函数进入线程池
	int on_data(tcp_unit* data) {
		//printf("the thread pid is %d\n", _getpid());
#ifdef _DEBUG
		int hl = data->headlen;
		//printf("the hl is %d\n", hl);
		//for (size_t i = hl; i < hl + 8; i++) {
		//	printf("%02x ", (uint8_t)data->data[i]);
		//}
		string test(data->bodydata, data->bodylen);
		printf("the data is %s\n", test.c_str());
		//printf("the len is %d\n", data->bodylen);
#endif



#if 0
		//给客户端回送信息

		tcp_unit_w* unitw = (tcp_unit_w*)malloc(sizeof(tcp_unit_w));
		unitw->delay = 0;
		unitw->data = (char*)malloc(sizeof(char) * 18);
		memcpy(unitw->data, buf, 18);
		unitw->len = 18;
		unitw->next = NULL;
		unitw->type = enum_sc;
		client_send(rb->id, unitw);
		//解析成为json发送到相应的接口
		string response;
		int timeout = 2; //2秒
						 //post是同步的

						 /*	bool ret = client.Post("http://127.0.0.1:9069/sensor_data/8052", buf, len, response, timeout);
						 if (ret == false) {
						 printf("error!:%s\n", client.geterror().c_str());
						 }
						 printf("response:%s\n", response.c_str());*/
#endif
		return 0;
	}


};