
#include "api.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*
xx xx xx xx  | xx xx xx xx    | xx xx xx xx   --  xx xx xx xx
4字节最大索引| 4字节当前索引    4bytes index    4bytes offset

data 
.....
*/

//2M的空间做索引
//uint32_t indext;//索引总长度
//uint32_t indexn;//索引长度,内容为x个索引 indexn <= indext

#define INDEX_BYTES 2 * 1024 * 1024
//int index_num = INDEX_BYTES / sizeof(int) * 2; //max index number is 162144
sdata_index* bfile_read_index(const char * file, uint32_t &indext,uint32_t &indexn)
{
	size_t nr;
	indext = 0;
	indexn = 0;
	FILE * fp = fopen(file, "rb");
	if (fp == NULL)
		return NULL;
	//读取最大的index值容量
	nr = fread(&indext, sizeof(uint32_t), 1, fp);
	if (nr == 0)
	{
		fclose(fp);
		return NULL;
	}
	//读取存储的索引值
	nr = fread(&indexn, sizeof(uint32_t), 1, fp);
	if (nr == 0)
	{
		fclose(fp);
		return NULL;
	}

	//4字节索引号 4字节偏移地址
	uint8_t * index = (uint8_t*)malloc(indexn * sizeof(uint32_t) * 2);
	nr = fread(index, indexn * sizeof(uint32_t) * 2, 1, fp);
	if (nr == 0)
	{
		fclose(fp);
		free(index);
		return NULL;
	}
	return (sdata_index*)index;
}

int Initialize(const char *tname, db_context *context)
{

	if (context == NULL)
		return -1;
	strcpy(context->v_tablename,tname);

	if (context->v_index == NULL)
		context->v_index = bfile_read_index(
			tname,             /*filename*/
			context->v_tindex, 
			context->v_nindex
		);

	if (context->v_index == NULL)
		return -1;
	return 0;
}


int Insert(void *data, db_context *context)
{
	if (context->v_nindex < context->v_tindex)
	{
		FILE * fp = fopen(context->v_tablename, "wb");
		if (fp == NULL)
			return -1;


		fpos_t pos = (fpos_t)(4 + 4 + (context->v_nindex) * (8));
		fsetpos(fp, &pos);
		//int last_index = context->v_nindex - 1;
		uint32_t  indexlast = context->v_index[context->v_nindex - 1].index ;
		uint32_t  indexinsert = indexlast + 1;
		size_t n = fwrite(&indexinsert, sizeof(uint32_t), 1, fp);
		if (n != sizeof(uint32_t))
		{
			fclose(fp);
			return -1;
		}

		sdata *pdata = (sdata*)data;

		size_t vsize = sizeof(pdata->vardata);
		size_t datasize = vsize * indexlast;
		fpos_t offset = 4 + 4 + INDEX_BYTES + datasize;
		n = fwrite(&offset, sizeof(uint32_t), 1, fp);
		if (n != sizeof(uint32_t))
		{
			fclose(fp);
			return -1;
		}

		fsetpos(fp, &offset);
		n = fwrite(data, vsize, 1, fp);
		if (n != vsize)
		{
			fclose(fp);
			return -1;
		}
		fpos_t index = sizeof(uint32_t);
		fsetpos(fp, &index);
		uint32_t newnindex = context->v_nindex + 1;
		n = fwrite(&newnindex, sizeof(uint32_t), 1, fp);
		if (n != sizeof(int))
		{
			fclose(fp);
			return -1;
		}
		fclose(fp);
		return 0;
	}
	return -1;
}


int   Delete(void *key, db_context *v_context)
{
	return 0;
}
int   Update(void *key, void *data, db_context *v_context)
{
	return 0;
}
void *Select(void *key, db_context *v_context)
{
	return NULL;
}

