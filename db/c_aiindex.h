#pragma once
/*
AI 文件得索引
test.db 对应 test.ai 文件
文件格式：
4字节 帧number

0000 0000 0000 0000  0    车
0000 0000 0000 0001  1    人脸
0000 0000 0000 0010  2    人群
0000 0000 0000 0100  4    车牌
0000 0000 0000 1000  8    武器
0000 0000 0001 0000  16   交通标识
0000 0000 0010 0000  32   
0000 0000 0100 0000  64
0000 0000 1000 0000  128
4字节 帧number 对应
4字节 物体识别种类个数   --->每2字节种类号码
           2 字节种类
		   2 字节种类
		   2字节种类
*/
#include <stdint.h>

#include <iostream>       // std::cout
#include <atomic>         // std::atomic
#include <thread>         // std::thread
#include <vector>         // std::vector

// a simple global linked list:
struct Node { int value; Node* next; };
std::atomic<Node*> list_head(nullptr);

void append(int val) {     // append an element to the list
	Node* oldHead = list_head;
	Node* newNode = new Node{ val,oldHead };

	// what follows is equivalent to: list_head = newNode, but in a thread-safe way:
	while (!list_head.compare_exchange_weak(oldHead, newNode))
		newNode->next = oldHead;
}

int testthread()
{
	// spawn 10 threads to fill the linked list:
	std::vector<std::thread> threads;
	for (int i = 0; i < 10; ++i) threads.push_back(std::thread(append, i));
	for (auto& th : threads) th.join();

	// print contents:
	for (Node* it = list_head; it != nullptr; it = it->next)
		std::cout << ' ' << it->value;
	std::cout << '\n';

	// cleanup:
	Node* it; while (it = list_head) { list_head = it->next; delete it; }

	return 0;
}



#if 0
struct buffer_data {
	uint8_t* ptr;
	size_t size;
};
static int read_packet(void* opaque, uint8_t* buf, int buf_size)
{
	struct buffer_data* bd = (struct buffer_data*)opaque;
	buf_size = FFMIN(buf_size, bd->size);

	if (!buf_size)
		return AVERROR_EOF;
	printf("ptr:%p size:%zu\n", bd->ptr, bd->size);

	memcpy(buf, bd->ptr, buf_size);
	bd->ptr += buf_size;
	bd->size -= buf_size;
	return buf_size;
}
int main(int argc, char* argv[])
{
	AVFormatContext* fmt_ctx = NULL;
	AVIOContext* avio_ctx = NULL;
	uint8_t* buffer = NULL, * avio_ctx_buffer = NULL;
	size_t buffer_size, avio_ctx_buffer_size = 4096;
	char* input_filename = NULL;
	int ret = 0;
	struct buffer_data bd = { 0 };

	input_filename = (char*)"D:/ps file/ps0.264";

	/* slurp file content into buffer */
	ret = av_file_map(input_filename, &buffer, &buffer_size, 0, NULL);
	if (ret < 0)
		return -1;

	/* fill opaque structure used by the AVIOContext read callback */
	bd.ptr = buffer;
	bd.size = buffer_size;
	if (!(fmt_ctx = avformat_alloc_context())) {
		ret = AVERROR(ENOMEM);
		return -1;
	}

	avio_ctx_buffer = (uint8_t*)av_malloc(avio_ctx_buffer_size);
	if (!avio_ctx_buffer) {
		ret = AVERROR(ENOMEM);
		return -1;
	}

	avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size,
		0, &bd, &read_packet, NULL, NULL);
	if (!avio_ctx) {
		ret = AVERROR(ENOMEM);
		return -1;
	}
	fmt_ctx->pb = avio_ctx;

	ret = avformat_open_input(&fmt_ctx, NULL, NULL, NULL);
	if (ret < 0) {
		std::cout << "Could not open input" << std::endl;
		return -1;
	}
	ret = avformat_find_stream_info(fmt_ctx, NULL);
	if (ret < 0) {
		std::cout << "Could not find stream information" << std::endl;
		return -1;
	}
	av_dump_format(fmt_ctx, 0, input_filename, 0);

	AVPacket pkt;
	av_read_frame(fmt_ctx, &pkt);

	avformat_close_input(&fmt_ctx);
	/* note: the internal buffer could have changed, and be != avio_ctx_buffer */
	if (avio_ctx) {
		av_freep(&avio_ctx->buffer);
		av_freep(&avio_ctx);
	}
	av_file_unmap(buffer, buffer_size);
	if (ret < 0) {
		std::cout << "Error occurred!" << std::endl;
		return 1;
	}
	return 0;
}
#endif