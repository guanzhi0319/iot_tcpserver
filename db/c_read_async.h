#pragma once
#include "api.h"
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <fcntl.h>

#ifndef _WIN32
#include <unistd.h>
#endif

#include <uv.h>
#ifdef  _WIN32
#include <WinSock2.h>
#include <windows.h>
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"psapi.lib")
#pragma comment(lib,"Iphlpapi.lib")
#pragma comment(lib,"Userenv.lib")
#endif


//unit test


typedef struct s_async_context
{
    uv_fs_t open_req;
    uv_fs_t read_req;
    uv_fs_t write_req;
    uv_async_t async;
}s_async_context;



void read_init_async(db_context* context, s_async_context* async, const char* fs);

//void read_context(s_async_context* context,);
//void uninit_read_context(s_async_context* context);