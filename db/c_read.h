#pragma once
#include <stdio.h>
#include <stdint.h>

#include "api.h"
int read_init(db_context* ctx, const char* db);
uint8_t* read_index(db_context* ctx, uint32_t index);
uint8_t* search_index(db_context* ctx, uint32_t ts);


int analyse_f(uint8_t* data, int len, db_context* ctx);

//from file read to out dfile
void read_file(const char* sfile, const char* dfile);
void read_network(uint16_t port, uint32_t ssrc);
void read_rtsp(const char* url, const char* dfile);
void read_gb28181();
//read onvif
void read_onvif();