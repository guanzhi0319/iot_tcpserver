#include "c_write.h"
#ifdef _WIN32
#include <io.h>
#define access _access
#else
#include <unistd.h>
#endif
#define HEAD_LEN 8
#define FP ctx->fp
///
//////
//index_num 总索引个数
int  write_init(db_context* ctx,const char * name ,uint32_t index_num)
{

	ctx->v_tindex = index_num;

	if (FP == NULL)
	{
		if (access(name, 0) == 0)
		{
			FP = fopen(name, "rb+");
			ctx->v_exists = 1;
		}
		else
		{
			FP = fopen(name, "wb+");
		}
		if (!FP)
			return -1;
	}
	int INDEX_BYTES = ctx->v_tindex *  sizeof(int64_t);
	if(ctx->fp == NULL)
		ctx->fp = fopen(ctx->v_tablename, "wb");
	if (ctx->fp == NULL)
		return -1;

	if (ctx->v_exists == 0)//not exists
	{

		fpos_t pos = (fpos_t)HEAD_LEN + INDEX_BYTES;//预留索引量
		pos -= 4; //退回4字节，写入0
		fsetpos(FP, &pos);
		int ts = 0;
		fwrite(&ts, sizeof(int), 1, FP);
			//fflush(FP);

		fpos_t pos_s = 0;
		fsetpos(FP, &pos_s);
		//写入总索引量
		fwrite(&index_num, sizeof(uint32_t), 1, FP);
		//写入目前索引号码，为零
		int index = 0;
		fwrite(&index, sizeof(int), 1, FP);
		fflush(FP);
		ctx->v_index_bytes = HEAD_LEN + INDEX_BYTES;
		ctx->v_exists = 1;//now exists
	}
	return 0;
}

int write_index_append(db_context* ctx, uint8_t* content,size_t len,uint32_t ts)
{
	fpos_t pos = 4;

	fsetpos(FP, &pos);

	//1 read the index number，读已存储索引数目
	int rindex = 0;
	size_t nr = fread(&rindex, sizeof(uint32_t), 1, FP);
	if (nr == 0)
	{
		//error
		//return -1;
	}
	//2 update write the index number = index +1
	rindex++;
	fsetpos(FP, &pos);
	fwrite(&rindex, sizeof(int), 1, FP);


	//3 write the content
	fseek(FP, 0, SEEK_END);
	fpos_t posnow;
	fgetpos(FP, &posnow);
	//4record the offset
	uint32_t offset_record = (uint32_t)posnow;
	//5 write the number of bytes for contents
	fwrite(&len, sizeof(int), 1, FP);
	//6 write the content
	fwrite(content, len, 1, FP);


	//7 update the offset
	fpos_t posindex = ((fpos_t)rindex-1) * 8;
	posindex = (4 + 4 + posindex);
	fsetpos(FP, &posindex);
	//write the index, write the offset
	if (ts != -1)
		fwrite(&rindex, sizeof(int), 1, FP);
	else
		fwrite(&ts, sizeof(int), 1, FP);
	fwrite(&offset_record, sizeof(int), 1, FP);
	fflush(FP);
	return 0;
}

//获取视频数据库基本状态信息
int status_read(db_context* ctx,const char* name)
{
	if (FP == NULL)
	{
		if (access(name, 0) == 0)
		{
			FP = fopen(name, "rb+");
			ctx->v_exists = 1;
		}
		else
		{
			ctx->v_exists = 0;
			return -1;
		}
        
	}
	//fpos_t pos = 0;

	//fsetpos(FP, &pos);

	//1 read the index number，读可存储索引数目
	int tindex, nindex = 0;
	size_t nr = fread(&tindex, sizeof(uint32_t), 1, FP);
	if (nr == 0)
	{
		//error
		return -1;
	}
	ctx->v_tindex = tindex;
	nr = fread(&nindex, sizeof(uint32_t), 1, FP);
	//2 read the already index number，读已经存储索引数目
	if (nr == 0)
	{
		return -1;
	}
	ctx->v_nindex = nindex;
	return 0;
}



int write_close(db_context* ctx)
{
	fclose(FP);
	FP = NULL;
	return 0;
}