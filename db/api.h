 #pragma once
#ifdef _WIN32
//#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <map>
//前缀8字节
//  4 字节总索引个数
//  4 字节已存储索引个数
//索引内容
// 4字节sequence num 或者timestamp   4字节偏移量
//文件内容
//  4字节内容长度， 内容不定长
typedef struct sdata
{
	uint32_t index;
	char vardata[128];
}sdata;

typedef struct sdata_index
{
	uint32_t index;
	uint32_t offset;
	sdata_index* next;
}sdata_index;
typedef struct db_context
{
	//总索引数目，由初始化写入决定
	uint32_t v_tindex = 0;
	uint32_t v_nindex = 0;//目前得索引
	uint32_t v_timestart = 0;//时间开始
	uint32_t v_timeend = 0;  //时间结束
	//file bytes index total 总索引加头部字节
	uint32_t v_index_bytes = 0;
	char v_tablename[256];
	FILE* fp = NULL;
	sdata_index* v_index = NULL;
	int v_exists = 0;
	uint8_t* v_data = NULL;
	int v_datalen = 0;
	//user 自定义context
	void* v_user_context = NULL;
	~db_context()
	{
		if (v_data != NULL)
		{
			free(v_data);
		}
		if (fp != NULL)
			fclose(fp);
	}
	//index map table
	std::map<uint32_t, uint32_t> v_index_maps;
	//index vector table
	std::vector<uint32_t> v_index_vector;
}db_context;

int   Insert(void *data,db_context *v_context);
int   Delete(void *key, db_context *v_context);
int   Update(void *key, void *data, db_context *v_context);
void *Select(void *key, db_context *v_context);





