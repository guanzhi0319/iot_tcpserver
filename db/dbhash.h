#ifndef MEMMAP_H   
#define MEMMAP_H   
#include <stdio.h>
#include <stdlib.h>
#include <unordered_map>
using namespace std;

#include <stdio.h>   

//class cmem_map
//{
//public:
//	cmem_map();
//	~cmem_map();
//
//	bool Map(const char* szFileName);
//	void UnMap();
//
//	const    void* GetData() const { return m_pData; }
//	size_t         GetSize() const { return m_uSize; }
//
//private:
//	void*     m_pData;
//	size_t    m_uSize;
//	int       m_nFile;
//};





#include <stdio.h>
#include <stdint.h>

#ifdef _WIN32
#include <Windows.h>
class cmem_map
{
	HANDLE hFile = NULL;
	OFSTRUCT opBuf;
	HANDLE hMapfile = NULL;
	HANDLE hMapview = NULL;
	long offset = 0;
public:
	cmem_map() {}
	~cmem_map() {}

	bool IsOpen()
	{
		return (hMapview != NULL);
	}
	bool IsClosed()
	{
		return (hMapfile == NULL);
	}
	int Open(const wchar_t * memfile_add, int fsize)
	{
		hFile = ::CreateFile(memfile_add, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == 0)
		{
			printf("open file failed!\n");
			return -1;
		}

		//2.为指定文件创建一个有名的文件映象
		hMapfile = CreateFileMapping((HANDLE)hFile, NULL, PAGE_READWRITE, 0, fsize, L"MapTest");
		if (hMapfile == NULL)
		{
			printf("mapping file failed!\n");
			return -1;
		}
		//关闭文件句柄
		CloseHandle((HANDLE)hFile);
		hFile = 0;
		hMapview = MapViewOfFile(hMapfile, FILE_MAP_WRITE, 0, 0, 0);
		if (hMapview == NULL)
		{
			printf("mapping view failed!\n");
			return -1;
		}
		return 0;
	}
	void Close()
	{
		UnmapViewOfFile(hMapview);
		//关闭文件映象句柄
		CloseHandle(hMapfile);
		hMapfile = 0;
	}
	long GetSize()
	{
		return offset;
	}
	int Write(uint8_t * data, int size)
	{
		char *recv = (char *)hMapview;
		recv += offset;
		memcpy(recv, data, size);
		offset += size;
		//把文件映射视图中的修改的内容或全部写回到磁盘文件中
		FlushViewOfFile(recv, size);
		return offset;
		//return 0;
	}
};

#endif  
#ifndef _WIN32
#include <fcntl.h>   
#include <unistd.h>   
#include <sys/mman.h>   



cmem_map::cmem_map() : m_pData(0), m_uSize(0), m_nFile(0)
{
}

cmem_map::~cmem_map()
{
	UnMap();
}

bool cmem_map::Map(const char* szFileName)
{
	UnMap();
	m_nFile = open(szFileName, O_RDONLY);
	if (m_nFile < 0)
	{
		m_nFile = 0;
		return false;
	}

	struct stat status;
	fstat(m_nFile, &status);

	m_uSize = status.st_size;
	m_pData = mmap(0, m_uSize, PROT_READ, MAP_SHARED, m_nFile, 0);
	if (MAP_FAILED != m_pData) { return true; }

	close(m_nFile);
	m_pData = NULL;
	m_nFile = 0;
	m_uSize = 0;
	return false;
}

void cmem_map::UnMap()
{
	if (m_pData)
	{
		munmap(m_pData, m_uSize);
		m_pData = NULL;
	}

	if (m_nFile)
	{
		close(m_nFile);
		m_nFile = 0;
	}

	m_uSize = 0;
}
#endif // !WIN32

#endif
