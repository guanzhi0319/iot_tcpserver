#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <chrono>
#include <algorithm>
#include <vector>
#include <iostream>
#include "api.h"
using namespace std;

//
//
//struct Rule {
//	bool operator()(const sdata &a1, const sdata &a2) {
//		return (a1.index) < (a2.index);
//	}
//};


static int binSearch_int(int x, int data[], int n)
{
	int low, high, mid;
	low = 0;
	high = n - 1;
	while (low <= high)
	{
		mid = (low + high) / 2;
		if (x < data[mid])
			high = mid - 1;
		else if (x > data[mid])
			low = mid + 1;
		else
			return mid;
	}
	//std::cout << "low is " << low << " high is" << high << std::endl;
	return -1;
}

/// <summary>
/// ����offsetƫ����
/// </summary>
/// <param name="ctx"></param>
/// <param name="ts"></param>
/// <returns>return  the offset</returns>
static uint32_t binSearch(db_context *ctx,uint32_t ts,uint32_t& low,uint32_t& high)
{
	low = 0, high = 0;
	uint32_t mid =0;
	
	high = (uint32_t)ctx->v_index_vector.size() -1;
	if (high == -1)
		return -1;
	if (high == 0)
		return ctx->v_index_vector[0];
	while (low <= high)
	{
		mid = (low + high) / 2;
		uint32_t tmp = ctx->v_index_vector[mid];
		if (ts < tmp)
			high = mid - 1;
		else if (ts > tmp)
			low = mid + 1;
		else
		{
			return  mid; // return �±�
		}
	}
	return -1;
}




