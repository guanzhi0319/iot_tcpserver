#include "c_read.h"
#include <stdio.h>
#include <stdlib.h>
#include "bsearch.h"
/// <summary>
/// 预读取得到索引到内存，record to 
/// </summary>
/// <param name="ctx">context</param>
/// <param name="db">filename</param>
int read_init(db_context* ctx,const char *db)
{
#define FP ctx->fp


	if (!ctx)
		return -1;
	if (!FP)
	{
		if ((FP = fopen(db, "rb")) == NULL)
			return -1;
	}
	//fpos_t pos = 4;
	//fsetpos(FP, &pos);
	uint32_t rindex = 0;

	fread(&ctx->v_tindex, sizeof(int), 1, FP);
	fread(&rindex, sizeof(int), 1, FP);

	if (rindex > ctx->v_tindex)
	{
		//error
		return -1;
	}
	// read all index to memory
	for (uint32_t i = 0; i < rindex; i++)
	{
		uint32_t ts; //四字节时间戳
		uint32_t offset;
		fread(&ts, sizeof(int), 1, FP);
		fread(&offset, sizeof(int), 1, FP);
		ctx->v_index_maps.insert(std::make_pair(ts, offset));
		//maybe we will do sort to use 
		ctx->v_index_vector.push_back(ts);
	}
	return 0;
}


uint8_t* read_index_0(db_context* ctx, uint32_t offset)
{
#define IP_LEN 65536
#define DATA ctx->v_data
#define DATALEN ctx->v_datalen
#define LOOK_NUMREAD(x) if(numread!=x)\
 return NULL


	//4 byte is length
	fpos_t pos = (fpos_t)offset;
	int length = 0;
	size_t numread;
	fsetpos(FP, &pos);
	numread = fread(&length, sizeof(int), 1, FP);
	LOOK_NUMREAD(1);
	if (length == 0)
		return NULL;

	//we ask the memory common in ip packet 64K,or the packet is more than 64k,then the actual length
	int memlen = (length > IP_LEN) ? length:IP_LEN;

	if (DATA == NULL)
		DATA = (uint8_t*)malloc(memlen);
	else {
		size_t dlen = sizeof(ctx->v_data);
		if ( dlen < memlen)
		{
			uint8_t* temp = (uint8_t*)realloc(DATA, (size_t)memlen);
			if (temp != NULL)
				DATA = temp;
			else
				return NULL;
		}
	}
	if (DATA == NULL)
		return NULL;
	numread = fread(DATA, length, 1, FP);
	LOOK_NUMREAD(1);
	DATALEN = length;
	return DATA;
}

uint8_t* read_index(db_context* ctx, uint32_t index)
{
#define MAP ctx->v_index_maps
	if (FP == NULL)
		return NULL;
	auto iter = MAP.find(index);
	if (iter == MAP.end())
		return NULL;
	uint32_t offset = iter->second;
	return read_index_0(ctx, offset);
}

/// <summary>
/// 使用二分法来快速搜索
/// </summary>
/// <param name="ctx"></param>
/// <param name="ts">输入模糊的timestamp</param>
/// <returns></returns>
uint8_t* search_index(db_context* ctx, uint32_t ts)
{
	if (!ctx || !ctx->fp)
		return NULL;

	uint32_t low, high;
	uint32_t mid = binSearch(ctx, ts, low, high);
	if (mid == -1)
	{
		uint32_t l = ctx->v_index_vector[low];
		uint32_t h = ctx->v_index_vector[high];
		uint32_t t1 = ts - l;
		uint32_t t2 = h - ts;
		if (t1 > t2)
			mid = high;
		else
			mid = low;
	}
	uint32_t index = ctx->v_index_vector[mid];
	return read_index(ctx, index);

}

/// <summary>
/// use ffmpeg to read file to save to out 
/// </summary>
/// <param name="file"></param>
/// <param name="tofile"></param>
void read_file(const char* file, const char* tofile)
{

}