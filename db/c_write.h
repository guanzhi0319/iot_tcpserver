#pragma once
#include <stdio.h>
#include <stdint.h>

#include "api.h"

//index_num       文件定义索引个数
//reserved_bytes  预留字节 ,default :10M
int write_init(db_context* ctx, const char* name, uint32_t index_num);

//ts 时间戳，如果时间戳为零，写入sequence number
int write_index_append(db_context* ctx, uint8_t* content,size_t len, uint32_t ts);

int write_close(db_context* ctx);

//获取视频数据库基本状态信息
int status_read(db_context* ctx,const char* name);